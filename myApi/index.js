const express = require('express')
var cors = require('cors')
const app = express()
const port = 3000

app.use(cors())

app.get('/', (req, res) => {
    const user= [
        {
            "id":1,
            "nama": "Septe Habudiin",
            "umur": 27
        },
        {
            "id":2,
            "nama": "Gustaman",
            "umur": 37
      }
    ]
    res.send(user)
 })

app.listen(port, () => console.log(`Example app listening on port ${port}!`))